<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Tampil Data Barang</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
    <h2>Tambah Barang</h2><br/>
      <div class="container">
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
    <!-- <br />
    <button class="btn btn-primary">Tambah Data</button>
    <br /> -->
    <br />
    <table class="table table-striped">
    <thead>
      <tr>
        <!--<th>ID</th>-->
        <th>Nama Barang</th>
        <th>Jenis Barang</th>
        <th>Harga Barang</th>
        <th>Jumlah Barang</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>
      
      @foreach($items as $item)
      <tr>
        <!--<td>{{$item->id}}</td>-->
        <td>{{$item->item_name}}</td>
        <td>{{$item->item_type}}</td>
        <td>{{$item->item_price}}</td>
        <td>{{$item->item_qty}}</td>
        <td><a href="{{action('ItemController@edit', $item->id)}}" class="btn btn-warning">Edit</a></td>
        <td>
          <form action="{{action('ItemController@destroy', $item->id)}}" method="post">
            @csrf
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  </body>
</html>