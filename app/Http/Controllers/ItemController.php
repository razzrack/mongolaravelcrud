<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();
        return view('itemindex',compact('items'));
        //    ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('items.create');
        return view('itemcreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = new Item();
        $item->item_name = $request->get('item_name');
        $item->item_type = $request->get('item_type');
        $item->item_price = $request->get('item_price');
        $item->item_qty = $request->get('item_qty');
        $item->save();
        return redirect('item')->with('success', 'Barang berhasil ditambahkan.');

        // request()->validate([
        //     'items_name' => 'required',
        //     'items_type' => 'required',
        //     'items_price' => 'required',
        //     'items_qty' => 'required',
        // ]);


        // Item::create($request->all());


        // return redirect()->route('items.index')
        //                 ->with('success','Barang berhasil ditambahkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    // public function show(Item $item)
    // {
    //     return view('items.show',compact('item'));
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id/*Item $item*/)
    {
        $item = Item::find($id);
        return view('itemedit', compact('item','id'));
        //return view('items.edit',compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id/*Item $item*/)
    {
        $item = Item::find($id);
        $item->item_price = $request->get('item_price');
        $item->item_qty = $request->get('item_qty');
        $item->save();
        return redirect('item')->with('success', 'Barang berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($id/*Item $item*/)
    {
        $item = Item::find($id);
        $item->delete();
        return redirect('item')->with('success','Barang berhasil dihapus.');
        // $item->delete();

        // return redirect()->route('items.index')
        //                 ->with('success','Barang berhasil dihapus.');
    }
}
