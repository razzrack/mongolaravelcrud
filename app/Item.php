<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Item extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'items';

    protected $fillable = [
        'item_name', 'item_type', 'item_price', 'item_qty'
    ];
}

?>