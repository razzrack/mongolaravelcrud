<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::resource('items','ItemController');
Route::get('add','ItemController@create');
Route::post('add','ItemController@store');
Route::get('item','ItemController@index');
Route::get('edit/{id}','ItemController@edit');
Route::post('edit/{id}','ItemController@update');
Route::delete('{id}','ItemController@destroy');